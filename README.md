# Inkscape Master Page Export

Export separate layers with Master Page layer always visible.
Semi functionality of "master page" in Inkscape.


## Install 

### Windows:

copy files to : `..your-inkscape-folder\share\extensions`

### Linux:

copy files to `~/.config/inkscape/extensions`

## Usage

You will find addon under `Effects/Export/Master Page Export`