#!/usr/bin/env python
import tempfile
import subprocess
import re
import os
import sys

import inkex
import simplestyle

# TODO add png, jpg, svg option
# TODO add timestamp text replace option

# Development
if sys.platform == "linux" or sys.platform == "linux2":
    sys.path.append('/usr/share/inkscape/extensions')
elif sys.platform == "darwin":
    pass
elif sys.platform == "win32":
    path = os.path.join(os.environ["ProgramFiles"],
                        'Inkscape', 'share', 'extensions')
    if os.path.exist(path):
        sys.path.append('/usr/share/inkscape/extensions')


def getLayers(element):
    """
    Get all groups (layers) from element.
    """
    # svg_layers = element.xpath('//svg:g[@inkscape:groupmode="layer"]', namespaces=inkex.NSS)
    # layers = []

    # for layer in svg_layers:
    #     label_attrib_name = "{%s}label" % layer.nsmap['inkscape']
    #     if label_attrib_name not in layer.attrib:
    #         continue

    #     layer_id = layer.attrib["id"]
    #     layer_label = layer.attrib[label_attrib_name]
    #     layers.append((layer_id, label, layer))

    layers = element.findall(
        ".//{http://www.w3.org/2000/svg}g[@{http://www.inkscape.org/namespaces/inkscape}groupmode='layer']")

    return layers


def layerDisplay(layer, displayed):
    """
    Hide or reveal layer.
    """
    if "style" in layer.attrib:
        style = simplestyle.parseStyle(layer.attrib["style"])
    else:
        style = {}

    if displayed:
        style["display"] = "inline"
    else:
        style["display"] = "none"

    layer.attrib["style"] = simplestyle.formatStyle(style)


def export_page(svg_path, output_path):
    """
    Saves full page pdf from svg.
    """
    # command = "inkscape -C -p \"%s\" \"%s\"" % (output_path, svg_path)
    command = 'inkscape -C -A "{}" "{}"'.format(output_path, svg_path)
    p = subprocess.Popen(command, shell=True,
                         stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    p.wait()


def merge_pages(input_files, output_name):
    try:
        from PyPDF2 import PdfFileReader, PdfFileWriter
    except ImportError:
        from pyPdf import PdfFileReader, PdfFileWriter

    if sys.platform == "win32":
        import os
        import msvcrt
        msvcrt.setmode(sys.stdout.fileno(), os.O_BINARY)

    input_streams = []
    try:
        # First open all the files, then produce the output file, and
        # finally close the input files. This is necessary because
        # the data isn't read from the input files until the write
        # operation. Thanks to
        # https://stackoverflow.com/questions/6773631/problem-with-closing-python-pypdf-writing-getting-a-valueerror-i-o-operation/6773733#6773733
        for input_file in input_files:
            input_streams.append(open(input_file, 'rb'))
        writer = PdfFileWriter()

        for reader in map(PdfFileReader, input_streams):
            for n in range(reader.getNumPages()):
                writer.addPage(reader.getPage(n))
        with open(output_name, 'wb') as out:
            writer.write(out)
    finally:
        for f in input_streams:
            f.close()


class MasterPageExport(inkex.Effect):
    """
    Example Inkscape effect extension.
    Creates a new layer with a "Hello World!" text centered in the middle of the document.
    """

    def __init__(self):
        """
        Constructor.
        """
        # Call the base class constructor.
        inkex.Effect.__init__(self)

        # Define string option "--what" with "-w" shortcut and default value "World".
        self.OptionParser.add_option('-o', '--output', action='store', type='string',
                                     dest='output', default='', help='Output folder path')
        self.OptionParser.add_option('-m', '--master', action='store', type='string',
                                     dest='master', default='Master', help='What layer is master page?')
        self.OptionParser.add_option("-d", "--today", action="store", type="inkbool",
                                     dest="today", default=False, help="Convert '%Y %m %d' signs to today date?  ex. 2011 12 23")
        self.OptionParser.add_option("-j", "--join", action="store", type="inkbool",
                                     dest="join", default=True, help="Merge pages into one pdf.[needs PyPDF2 in inkscape python]")

    def getMaster(self, layers):
        """
        Returns master layer if in layers.
        """
        master = self.options.master

        for nr, l in enumerate(layers):
            label = l.attrib["{http://www.inkscape.org/namespaces/inkscape}label"]
            if label == master:
                return nr
        return None

    def effect(self):
        svg = self.document.getroot()
        filename = svg.attrib['{http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd}docname']
        filename = os.path.splitext(filename)[0]
        output_path = self.options.output
        today = self.options.today
        join = self.options.join
        layers = getLayers(self.document)
        master_idx = self.getMaster(layers)
        master = layers.pop(master_idx)
        layerDisplay(master, True)
        #  prepare for printing
        for l in layers:
            layerDisplay(l, False)
        # printing
        filepaths = []
        for l in layers:
            label = l.attrib["{http://www.inkscape.org/namespaces/inkscape}label"]
            output_file = os.path.join(
                output_path, '{}_{}.pdf'.format(filename, label))
            filepaths.append(output_file)
            layerDisplay(l, True)
            if today:
                rename_date()
            self.document.write('tmp.svg')
            print(output_path)
            export_page('tmp.svg', output_file)
            layerDisplay(l, False)
        os.remove('tmp.svg')
        if join:
            merge_pages(filepaths, os.path.join(
                output_path, filename + '.pdf'))


effect = MasterPageExport()
effect.affect()
